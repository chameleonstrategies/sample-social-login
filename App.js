/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {SafeAreaView, ScrollView, View, StatusBar} from 'react-native';

import LoginButton from './LoginButtons';

const App = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <View>
            <LoginButton />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default App;
