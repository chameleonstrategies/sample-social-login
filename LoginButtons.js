import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Button,
  TouchableOpacity,
} from 'react-native';
import ApolloClient, {gql} from 'apollo-boost';
import {LoginManager, AccessToken} from 'react-native-fbsdk';
import {GoogleSignin} from 'react-native-google-signin';

const client = new ApolloClient({
  uri: 'https://api.enwoke.com/graphql?key=F51OUlSm4pmk7FsAQ2PKKY5G1mku5MpG',
});

GoogleSignin.configure({
  webClientId:
    '860623996075-p938t43k6lritru240g86s6gd71enb8e.apps.googleusercontent.com',
  offlineAccess: true,
});

class LoginButton extends React.Component {
  constructor() {
    super();
    this.state = {
      token: null,
      user: null,
      isLoggedIn: false,
    };

    this.loginFacebook = this.loginFacebook.bind(this);
    this.loginGoogle = this.loginGoogle.bind(this);
    this.logout = this.logout.bind(this);
  }

  async loginFacebook() {
    LoginManager.logInWithPermissions(['public_profile', 'email'])
      .then(result => {
        if (result.isCancelled) {
          console.log('User cancelled.');
        } else {
          AccessToken.getCurrentAccessToken()
            .then(data => {
              const {accessToken} = data;
              client
                .mutate({
                  mutation: gql`
                    mutation appUserLoginWithFacebook($accessToken: String!) {
                      appUserLoginWithFacebook(accessToken: $accessToken) {
                        token
                        user {
                          id
                          firstName
                          lastName
                          email
                          picture
                        }
                      }
                    }
                  `,
                  variables: {
                    accessToken,
                  },
                })
                .then(res => {
                  const {token, user} = res.data.appUserLoginWithFacebook;
                  this.setState({token, user});
                });
            })
            .catch(console.log);
        }
      })
      .catch(console.log);
  }

  async loginGoogle() {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      const {idToken} = userInfo;

      client
        .mutate({
          mutation: gql`
            mutation appUserLoginWithGoogle($idToken: String!) {
              appUserLoginWithGoogle(idToken: $idToken) {
                token
                user {
                  id
                  firstName
                  lastName
                  email
                  picture
                }
              }
            }
          `,
          variables: {
            idToken,
          },
        })
        .then(res => {
          const {token, user} = res.data.appUserLoginWithGoogle;
          this.setState({token, user});
        });
    } catch (error) {
      console.log(error);
    }
  }

  logout() {
    this.setState({
      user: null,
      token: null,
    });
  }

  render() {
    return (
      <>
        <View style={styles.container}>
          <Text style={styles.text}>Social Login Sample</Text>
          <View style={styles.socialContainer}>
            {this.state.user ? (
              <>
                <Text style={styles.welcomeText}>
                  Hello, {this.state.user.firstName}
                </Text>
                <Button onPress={this.logout} title="Logout" />
              </>
            ) : (
              <>
                <TouchableOpacity onPress={this.loginFacebook}>
                  <Image
                    source={require('./img/facebook.png')}
                    width={24}
                    height={24}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => console.log('Pressed!')}>
                  <Image
                    source={require('./img/twitter.png')}
                    width={24}
                    height={24}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={this.loginGoogle}>
                  <Image
                    source={require('./img/google-plus.png')}
                    width={24}
                    height={24}
                  />
                </TouchableOpacity>
              </>
            )}
          </View>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 150,
  },
  text: {
    fontWeight: 'bold',
    fontSize: 30,
    textAlign: 'center',
  },
  welcomeText: {
    fontSize: 20,
    textAlign: 'center',
  },
  socialContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 100,
    paddingLeft: 50,
    paddingRight: 50,
  },
});

export default LoginButton;
